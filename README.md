- Install Python3 

- Clone  this repository 

- Install prerequisites
`pip install -r requirements.txt`

- Amazon Credentials: 
  - create file ~/.aws/credentials


> [default]

> aws_access_key_id = <aws_access_key_id>

> aws_secret_access_key = <aws_secret_access_key>

> region = aws_region


- Launch update 
python main.py 