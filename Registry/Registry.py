import winreg
import os
from ssh_config import client
class Registry():
    REG_PATH = 'Software\SimonTatham\PuTTY\Sessions'
    client.SSHConfig
    def setIP(self,ipAddress):
        computer_name = os.environ['COMPUTERNAME']
        key = 'HKEY_CURRENT_USER\Software\SimonTatham\PuTTY\Sessions'
        reg = winreg.ConnectRegistry(computer_name, key)

    def set_reg(self,sessionName, ipAddress):
        try:
            reg_path = "{}\{}".format(self.REG_PATH, sessionName)
            winreg.CreateKey(winreg.HKEY_CURRENT_USER, self.REG_PATH)
            registry_key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, reg_path, 0,
                                           winreg.KEY_WRITE)
            winreg.SetValueEx(registry_key, 'HostName', 0, winreg.REG_SZ, ipAddress)
            winreg.CloseKey(registry_key)
            return True
        except WindowsError:
            return False

    def get_reg(self,sessionName):
        try:
            reg_path = "{}\{}".format(self.REG_PATH,sessionName)
            registry_key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, reg_path, 0,
                                           winreg.KEY_READ)
            value, regtype = winreg.QueryValueEx(registry_key, 'HostName')
            winreg.CloseKey(registry_key)
            return value
        except WindowsError:
            return None