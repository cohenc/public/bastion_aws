import boto3
import pprint
import time
class AWS:
    def __init__(self):
        self.client = boto3.client('ec2')
        self.conn = boto3.resource('ec2')

    def startInstance(self,instanceID, instanceName):
        print('Starting instance: {}'.format(instanceName))
        self.client.start_instances(InstanceIds=[instanceID])
        time.sleep(30)

    def getInstanceByName(self,name):

        names = [ name ]

        custom_filter = [
            {
                'Name': 'tag:Name',
                'Values': names
            }
        ]

        response = self.client.describe_instances(Filters=custom_filter)
        if response['Reservations'][0]['Instances'][0]['State']['Name'] == 'stopped':
            self.startInstance(response['Reservations'][0]['Instances'][0]['InstanceId'], name)
            
        return response['Reservations'][0]['Instances']

    def getPublicIP(self,instanceName):
        instance = self.getInstanceByName(instanceName)
        if instance:
            return instance[0]['NetworkInterfaces'][0]['PrivateIpAddresses'][0]['Association']['PublicIp']
        else:
            return False