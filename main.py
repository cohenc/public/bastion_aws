#!/usr/bin/env python
#-*- coding: utf-8 -*-


import pprint
import boto3
from AWS import AWS

from platform import system
import subprocess
import sys

def main():
    aws = AWS.AWS()

    SESSION_NAME = 'AWS'
    BASTION_NAME = 'I2S-OKD-BASTION'

    ip_address = aws.getPublicIP(BASTION_NAME)
    if ip_address:
        print("AWS Public IP Address is : {}\n".format(ip_address))
        if system() == 'Windows':
            from Registry import Registry
            registry = Registry.Registry()
            print("Updating AWS Session registry...\n")
            if (registry.set_reg(SESSION_NAME,ip_address)):
                print("...OK!\n")
            else:
                print("Oops an error occured...")
                sys.exit(1)

        # Mise à jour du fichier ~/.ssh/config Windows & Linux
        print("Updating ~/.ssh/config \n")
        subprocess.run(["ssh-config", "update", SESSION_NAME, "HostName={}".format(ip_address), "--yes"])
    else:
        print("No Ip Address detected, something went wrong\n")


if __name__ == "__main__":
    # execute only if run as a script
    main()
